package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import ru.potapov.tm.endpoint.*;

public interface IProjectService extends IService {
    int checkSize()  throws ValidateExeption_Exception;
    @Nullable Project findOne(@NotNull final String name)  throws ValidateExeption_Exception;
    @Nullable Project findProjectByName(@NotNull final String name) throws ValidateExeption_Exception;
    @Nullable Project findProjectByName(@NotNull final String userId, @NotNull final String name) throws ValidateExeption_Exception;
    @Nullable Project findOneById(@NotNull final String id) throws ValidateExeption_Exception;
    @Nullable Project findOneById(@NotNull final String userId, @NotNull final String id) throws ValidateExeption_Exception;
    @NotNull Collection<Project> getCollection(@NotNull final String userId) throws ValidateExeption_Exception;
    @NotNull Project renameProject(@NotNull final Project project, @NotNull final String name) throws CloneNotSupportedException_Exception, ValidateExeption_Exception;
    void removeAll(@NotNull final String userId) throws ValidateExeption_Exception;
    void removeAll(Collection<Project> listProjects) throws ValidateExeption_Exception;
    void remove(@NotNull final Project project) throws ValidateExeption_Exception;
    void put(@NotNull final Project project) throws ValidateExeption_Exception;
    @NotNull String collectProjectInfo(@NotNull final Project project, @NotNull final String owener) throws ValidateExeption_Exception;
    //void setMapRepository(@NotNull final Map<String, Project> mapRepository);
    //@NotNull Map<String, Project> getMapRepository();
}
