package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.command.AbstractCommand;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;

public interface ITerminalService {
    void initCommands(final Class[] CLASSES);
    void regestry(@NotNull final AbstractCommand command);
    @NotNull Collection<AbstractCommand> getListCommands();
    @NotNull Map<String, AbstractCommand> getMapCommands();
    @Nullable Date inputDate(@NotNull final String massage);
    @Nullable public XMLGregorianCalendar DateToXml(Date date);
    @NotNull Scanner getIn();
    @NotNull String readLine(@NotNull final String msg);
    void printMassageNotAuthorized();
    void printMassageCompleted();
    void printMassageOk();
    void printlnArbitraryMassage(@NotNull final String msg);
    void printArbitraryMassage(@NotNull final String msg);
}
