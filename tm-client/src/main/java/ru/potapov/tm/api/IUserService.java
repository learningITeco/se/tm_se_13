package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.endpoint.*;

import java.lang.Exception;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.Map;

public interface IUserService{
    @NotNull User create(@NotNull final String name, @NotNull final String hashPass, @NotNull final RoleType role);
    @Nullable User getUserByName(@NotNull final String name);
    @Nullable User getUserById(@NotNull final String id) throws ValidateExeption_Exception;
    @NotNull Collection<User> getCollectionUser();
    @NotNull User changePass(@NotNull final User user, @NotNull final String newHashPass) throws CloneNotSupportedException_Exception, ValidateExeption_Exception;
    void put(@NotNull final User user) throws ValidateExeption_Exception;
    @NotNull String collectUserInfo(@NotNull final User user) throws ValidateExeption_Exception;
    @Nullable MessageDigest getMd();
    boolean isAuthorized();
    void setAuthorized(boolean authorized);
    @Nullable User getAuthorizedUser();
    void setAuthorizedUser(@NotNull final User authorizedUser);
    void saveAllUsers() throws Exception;
    void loadAllUsers() throws Exception;
    //@Nullable User getUserByNameAndPass(@Nullable Session session, @NotNull final String name, @NotNull final String pass);
    @Nullable User getUserByNamePass(@NotNull final String name, @NotNull final String pass) throws Exception_Exception;
    int size();
    @Nullable RoleType getUserRole(@NotNull Session session, @NotNull final User user)  throws ValidateExeption_Exception;
    boolean isAdministrator(@NotNull final User user)  throws ValidateExeption_Exception;
}
