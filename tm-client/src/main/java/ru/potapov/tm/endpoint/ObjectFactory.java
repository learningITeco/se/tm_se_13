
package ru.potapov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.potapov.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CloneNotSupportedException_QNAME = new QName("http://endpoint.tm.potapov.ru/", "CloneNotSupportedException");
    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.potapov.ru/", "Exception");
    private final static QName _ValidateExeption_QNAME = new QName("http://endpoint.tm.potapov.ru/", "ValidateExeption");
    private final static QName _ChangePass_QNAME = new QName("http://endpoint.tm.potapov.ru/", "changePass");
    private final static QName _ChangePassResponse_QNAME = new QName("http://endpoint.tm.potapov.ru/", "changePassResponse");
    private final static QName _CollectUserInfo_QNAME = new QName("http://endpoint.tm.potapov.ru/", "collectUserInfo");
    private final static QName _CollectUserInfoResponse_QNAME = new QName("http://endpoint.tm.potapov.ru/", "collectUserInfoResponse");
    private final static QName _CreateUser_QNAME = new QName("http://endpoint.tm.potapov.ru/", "createUser");
    private final static QName _CreateUserResponse_QNAME = new QName("http://endpoint.tm.potapov.ru/", "createUserResponse");
    private final static QName _GetUserById_QNAME = new QName("http://endpoint.tm.potapov.ru/", "getUserById");
    private final static QName _GetUserByIdResponse_QNAME = new QName("http://endpoint.tm.potapov.ru/", "getUserByIdResponse");
    private final static QName _GetUserByName_QNAME = new QName("http://endpoint.tm.potapov.ru/", "getUserByName");
    private final static QName _GetUserByNamePass_QNAME = new QName("http://endpoint.tm.potapov.ru/", "getUserByNamePass");
    private final static QName _GetUserByNamePassResponse_QNAME = new QName("http://endpoint.tm.potapov.ru/", "getUserByNamePassResponse");
    private final static QName _GetUserByNameResponse_QNAME = new QName("http://endpoint.tm.potapov.ru/", "getUserByNameResponse");
    private final static QName _GetUserCollection_QNAME = new QName("http://endpoint.tm.potapov.ru/", "getUserCollection");
    private final static QName _GetUserCollectionResponse_QNAME = new QName("http://endpoint.tm.potapov.ru/", "getUserCollectionResponse");
    private final static QName _GetUserRole_QNAME = new QName("http://endpoint.tm.potapov.ru/", "getUserRole");
    private final static QName _GetUserRoleResponse_QNAME = new QName("http://endpoint.tm.potapov.ru/", "getUserRoleResponse");
    private final static QName _IsAdministrator_QNAME = new QName("http://endpoint.tm.potapov.ru/", "isAdministrator");
    private final static QName _IsAdministratorResponse_QNAME = new QName("http://endpoint.tm.potapov.ru/", "isAdministratorResponse");
    private final static QName _PutUser_QNAME = new QName("http://endpoint.tm.potapov.ru/", "putUser");
    private final static QName _PutUserResponse_QNAME = new QName("http://endpoint.tm.potapov.ru/", "putUserResponse");
    private final static QName _SaveAllUsers_QNAME = new QName("http://endpoint.tm.potapov.ru/", "saveAllUsers");
    private final static QName _SaveAllUsersResponse_QNAME = new QName("http://endpoint.tm.potapov.ru/", "saveAllUsersResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.potapov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CloneNotSupportedException }
     * 
     */
    public CloneNotSupportedException createCloneNotSupportedException() {
        return new CloneNotSupportedException();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link ValidateExeption }
     * 
     */
    public ValidateExeption createValidateExeption() {
        return new ValidateExeption();
    }

    /**
     * Create an instance of {@link ChangePass }
     * 
     */
    public ChangePass createChangePass() {
        return new ChangePass();
    }

    /**
     * Create an instance of {@link ChangePassResponse }
     * 
     */
    public ChangePassResponse createChangePassResponse() {
        return new ChangePassResponse();
    }

    /**
     * Create an instance of {@link CollectUserInfo }
     * 
     */
    public CollectUserInfo createCollectUserInfo() {
        return new CollectUserInfo();
    }

    /**
     * Create an instance of {@link CollectUserInfoResponse }
     * 
     */
    public CollectUserInfoResponse createCollectUserInfoResponse() {
        return new CollectUserInfoResponse();
    }

    /**
     * Create an instance of {@link CreateUser }
     * 
     */
    public CreateUser createCreateUser() {
        return new CreateUser();
    }

    /**
     * Create an instance of {@link CreateUserResponse }
     * 
     */
    public CreateUserResponse createCreateUserResponse() {
        return new CreateUserResponse();
    }

    /**
     * Create an instance of {@link GetUserById }
     * 
     */
    public GetUserById createGetUserById() {
        return new GetUserById();
    }

    /**
     * Create an instance of {@link GetUserByIdResponse }
     * 
     */
    public GetUserByIdResponse createGetUserByIdResponse() {
        return new GetUserByIdResponse();
    }

    /**
     * Create an instance of {@link GetUserByName }
     * 
     */
    public GetUserByName createGetUserByName() {
        return new GetUserByName();
    }

    /**
     * Create an instance of {@link GetUserByNamePass }
     * 
     */
    public GetUserByNamePass createGetUserByNamePass() {
        return new GetUserByNamePass();
    }

    /**
     * Create an instance of {@link GetUserByNamePassResponse }
     * 
     */
    public GetUserByNamePassResponse createGetUserByNamePassResponse() {
        return new GetUserByNamePassResponse();
    }

    /**
     * Create an instance of {@link GetUserByNameResponse }
     * 
     */
    public GetUserByNameResponse createGetUserByNameResponse() {
        return new GetUserByNameResponse();
    }

    /**
     * Create an instance of {@link GetUserCollection }
     * 
     */
    public GetUserCollection createGetUserCollection() {
        return new GetUserCollection();
    }

    /**
     * Create an instance of {@link GetUserCollectionResponse }
     * 
     */
    public GetUserCollectionResponse createGetUserCollectionResponse() {
        return new GetUserCollectionResponse();
    }

    /**
     * Create an instance of {@link GetUserRole }
     * 
     */
    public GetUserRole createGetUserRole() {
        return new GetUserRole();
    }

    /**
     * Create an instance of {@link GetUserRoleResponse }
     * 
     */
    public GetUserRoleResponse createGetUserRoleResponse() {
        return new GetUserRoleResponse();
    }

    /**
     * Create an instance of {@link IsAdministrator }
     * 
     */
    public IsAdministrator createIsAdministrator() {
        return new IsAdministrator();
    }

    /**
     * Create an instance of {@link IsAdministratorResponse }
     * 
     */
    public IsAdministratorResponse createIsAdministratorResponse() {
        return new IsAdministratorResponse();
    }

    /**
     * Create an instance of {@link PutUser }
     * 
     */
    public PutUser createPutUser() {
        return new PutUser();
    }

    /**
     * Create an instance of {@link PutUserResponse }
     * 
     */
    public PutUserResponse createPutUserResponse() {
        return new PutUserResponse();
    }

    /**
     * Create an instance of {@link SaveAllUsers }
     * 
     */
    public SaveAllUsers createSaveAllUsers() {
        return new SaveAllUsers();
    }

    /**
     * Create an instance of {@link SaveAllUsersResponse }
     * 
     */
    public SaveAllUsersResponse createSaveAllUsersResponse() {
        return new SaveAllUsersResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CloneNotSupportedException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "CloneNotSupportedException")
    public JAXBElement<CloneNotSupportedException> createCloneNotSupportedException(CloneNotSupportedException value) {
        return new JAXBElement<CloneNotSupportedException>(_CloneNotSupportedException_QNAME, CloneNotSupportedException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateExeption }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "ValidateExeption")
    public JAXBElement<ValidateExeption> createValidateExeption(ValidateExeption value) {
        return new JAXBElement<ValidateExeption>(_ValidateExeption_QNAME, ValidateExeption.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePass }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "changePass")
    public JAXBElement<ChangePass> createChangePass(ChangePass value) {
        return new JAXBElement<ChangePass>(_ChangePass_QNAME, ChangePass.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePassResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "changePassResponse")
    public JAXBElement<ChangePassResponse> createChangePassResponse(ChangePassResponse value) {
        return new JAXBElement<ChangePassResponse>(_ChangePassResponse_QNAME, ChangePassResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CollectUserInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "collectUserInfo")
    public JAXBElement<CollectUserInfo> createCollectUserInfo(CollectUserInfo value) {
        return new JAXBElement<CollectUserInfo>(_CollectUserInfo_QNAME, CollectUserInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CollectUserInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "collectUserInfoResponse")
    public JAXBElement<CollectUserInfoResponse> createCollectUserInfoResponse(CollectUserInfoResponse value) {
        return new JAXBElement<CollectUserInfoResponse>(_CollectUserInfoResponse_QNAME, CollectUserInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "createUser")
    public JAXBElement<CreateUser> createCreateUser(CreateUser value) {
        return new JAXBElement<CreateUser>(_CreateUser_QNAME, CreateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "createUserResponse")
    public JAXBElement<CreateUserResponse> createCreateUserResponse(CreateUserResponse value) {
        return new JAXBElement<CreateUserResponse>(_CreateUserResponse_QNAME, CreateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "getUserById")
    public JAXBElement<GetUserById> createGetUserById(GetUserById value) {
        return new JAXBElement<GetUserById>(_GetUserById_QNAME, GetUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "getUserByIdResponse")
    public JAXBElement<GetUserByIdResponse> createGetUserByIdResponse(GetUserByIdResponse value) {
        return new JAXBElement<GetUserByIdResponse>(_GetUserByIdResponse_QNAME, GetUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "getUserByName")
    public JAXBElement<GetUserByName> createGetUserByName(GetUserByName value) {
        return new JAXBElement<GetUserByName>(_GetUserByName_QNAME, GetUserByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserByNamePass }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "getUserByNamePass")
    public JAXBElement<GetUserByNamePass> createGetUserByNamePass(GetUserByNamePass value) {
        return new JAXBElement<GetUserByNamePass>(_GetUserByNamePass_QNAME, GetUserByNamePass.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserByNamePassResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "getUserByNamePassResponse")
    public JAXBElement<GetUserByNamePassResponse> createGetUserByNamePassResponse(GetUserByNamePassResponse value) {
        return new JAXBElement<GetUserByNamePassResponse>(_GetUserByNamePassResponse_QNAME, GetUserByNamePassResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "getUserByNameResponse")
    public JAXBElement<GetUserByNameResponse> createGetUserByNameResponse(GetUserByNameResponse value) {
        return new JAXBElement<GetUserByNameResponse>(_GetUserByNameResponse_QNAME, GetUserByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserCollection }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "getUserCollection")
    public JAXBElement<GetUserCollection> createGetUserCollection(GetUserCollection value) {
        return new JAXBElement<GetUserCollection>(_GetUserCollection_QNAME, GetUserCollection.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserCollectionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "getUserCollectionResponse")
    public JAXBElement<GetUserCollectionResponse> createGetUserCollectionResponse(GetUserCollectionResponse value) {
        return new JAXBElement<GetUserCollectionResponse>(_GetUserCollectionResponse_QNAME, GetUserCollectionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "getUserRole")
    public JAXBElement<GetUserRole> createGetUserRole(GetUserRole value) {
        return new JAXBElement<GetUserRole>(_GetUserRole_QNAME, GetUserRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "getUserRoleResponse")
    public JAXBElement<GetUserRoleResponse> createGetUserRoleResponse(GetUserRoleResponse value) {
        return new JAXBElement<GetUserRoleResponse>(_GetUserRoleResponse_QNAME, GetUserRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsAdministrator }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "isAdministrator")
    public JAXBElement<IsAdministrator> createIsAdministrator(IsAdministrator value) {
        return new JAXBElement<IsAdministrator>(_IsAdministrator_QNAME, IsAdministrator.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsAdministratorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "isAdministratorResponse")
    public JAXBElement<IsAdministratorResponse> createIsAdministratorResponse(IsAdministratorResponse value) {
        return new JAXBElement<IsAdministratorResponse>(_IsAdministratorResponse_QNAME, IsAdministratorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PutUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "putUser")
    public JAXBElement<PutUser> createPutUser(PutUser value) {
        return new JAXBElement<PutUser>(_PutUser_QNAME, PutUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PutUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "putUserResponse")
    public JAXBElement<PutUserResponse> createPutUserResponse(PutUserResponse value) {
        return new JAXBElement<PutUserResponse>(_PutUserResponse_QNAME, PutUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveAllUsers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "saveAllUsers")
    public JAXBElement<SaveAllUsers> createSaveAllUsers(SaveAllUsers value) {
        return new JAXBElement<SaveAllUsers>(_SaveAllUsers_QNAME, SaveAllUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveAllUsersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.potapov.ru/", name = "saveAllUsersResponse")
    public JAXBElement<SaveAllUsersResponse> createSaveAllUsersResponse(SaveAllUsersResponse value) {
        return new JAXBElement<SaveAllUsersResponse>(_SaveAllUsersResponse_QNAME, SaveAllUsersResponse.class, null, value);
    }

}
