package ru.potapov.tm.command.terminal;

public enum SortBy {
    ByCreate,
    ByDateStart,
    ByDateFinish,
    ByStatus;
}
