package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;

import java.lang.Exception;
import java.util.Collection;
import java.util.Objects;
import ru.potapov.tm.endpoint.*;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectDeleteCommand extends AbstractCommand {
    public ProjectDeleteCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        setNeedAuthorize(true);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-delete";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Deletes the project";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        @Nullable Project findProject = null;
        boolean circleForProject = true;
        while (circleForProject) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage("Input a project name for removing:");
            @NotNull String name = getServiceLocator().getTerminalService().getIn().nextLine();

            if ("exit".equals(name)){
                return;
            }

            findProject = getServiceLocator().getProjectService().findProjectByName(name);

            if (Objects.isNull(findProject)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            if (!getServiceLocator().getUserService().getAuthorizedUser().getId().equals(findProject.getUserId())){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not belong to you!, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }

        @NotNull Collection<Task> listTaskRemoving = getServiceLocator().getTaskService().findAll(findProject.getId());
        getServiceLocator().getTaskService().removeAll(listTaskRemoving);
        getServiceLocator().getProjectService().remove(findProject);
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
