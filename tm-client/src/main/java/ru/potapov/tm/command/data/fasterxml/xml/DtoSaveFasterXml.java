package ru.potapov.tm.command.data.fasterxml.xml;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;

import java.lang.Exception;
import java.util.Objects;
import ru.potapov.tm.endpoint.*;

@NoArgsConstructor
public class DtoSaveFasterXml extends AbstractCommand {
    public DtoSaveFasterXml(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public @NotNull String getName() {
        return "save-faster-xml";
    }

    @Override
    public @NotNull String getDescription() {
        return "Saves in xml-format by FasterXML";
    }

    @Override
    public void execute(@Nullable String... additionalCommands) throws Exception {
        @Nullable final User user = getServiceLocator().getUserService().getAuthorizedUser();
        if (Objects.isNull(user) || !getServiceLocator().getUserService().isAdministrator( user ))
            return;
        if (!super.allowedRun())
            return;

        getServiceLocator().getProjectService().saveFasterXml(getServiceLocator().getSessionService().getSession());
        getServiceLocator().getTaskService().saveFasterXml(getServiceLocator().getSessionService().getSession());
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
