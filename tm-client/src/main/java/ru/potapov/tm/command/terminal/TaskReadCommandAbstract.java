package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;
import ru.potapov.tm.endpoint.*;

import java.lang.Exception;
import java.util.Collection;
import java.util.Comparator;
import java.util.Objects;

@NoArgsConstructor
@Setter
@Getter
public abstract class TaskReadCommandAbstract extends AbstractCommand {
    @NotNull SortBy sortBy                = SortBy.ByCreate;
    @Nullable String partOfNameOrDiscript = null;

    public TaskReadCommandAbstract(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        setNeedAuthorize(true);
    }

    @NotNull
    @Override
    public abstract String getName();

    @NotNull
    @Override
    public abstract String getDescription();

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;

        if (Objects.nonNull(additionalCommands)){
            if (additionalCommands.length > 1){
                if ("-c".equals(additionalCommands[1])){
                    sortBy = SortBy.ByCreate;
                }else if ("-ds".equals(additionalCommands[1])){
                    sortBy = SortBy.ByDateStart;
                }else if ("-df".equals(additionalCommands[1])){
                    sortBy = SortBy.ByDateFinish;
                }else if ("-s".equals(additionalCommands[1])){
                    sortBy = SortBy.ByStatus;
                }
                if (additionalCommands.length > 2){
                    if ("-f".equals(additionalCommands[1])){
                        partOfNameOrDiscript = additionalCommands[2];
                    }
                }
            }
        }
    };

    public void printTasksOfProject(@NotNull Project project, @NotNull Collection<Task> listTasks)  throws ValidateExeption_Exception {
        if (Objects.isNull(getServiceLocator()))
            return;

        getServiceLocator().getTerminalService().printlnArbitraryMassage("");
        getServiceLocator().getTerminalService().printlnArbitraryMassage("Project [" + project.getName() + "]:");
        for (Task task : listTasks) {
            if (Objects.isNull(task.getProjectId()))
                continue;

            if (Objects.nonNull(partOfNameOrDiscript)) {
                if (Objects.isNull(task.getName()) || Objects.isNull(task.getDescription()))
                    continue;
                if (!partOfNameOrDiscript.equals("") && ((!task.getName().contains(partOfNameOrDiscript)) && (!task.getDescription().contains(partOfNameOrDiscript))))
                    continue;
            }

            if (task.getProjectId().equals(project.getId())){
                @NotNull String taskInfo =  getServiceLocator().getTaskService().collectTaskInfo(task);
                getServiceLocator().getTerminalService().printlnArbitraryMassage(taskInfo);
            }
        }
    }

    public void printTasks(@NotNull Collection<Task> listTasks) throws ValidateExeption_Exception {
        if (Objects.isNull(getServiceLocator()))
            return;

        for (Task task : listTasks) {
            if (Objects.nonNull(partOfNameOrDiscript)) {
                if (Objects.isNull(task.getName()) || Objects.isNull(task.getDescription()))
                    continue;
                if (!partOfNameOrDiscript.equals("") && ((!task.getName().contains(partOfNameOrDiscript)) || (!task.getDescription().contains(partOfNameOrDiscript))))
                    continue;
            }

            @NotNull String taskInfo =  getServiceLocator().getTaskService().collectTaskInfo(task);
            getServiceLocator().getTerminalService().printlnArbitraryMassage(taskInfo);
        }
    }

    @NotNull
    public Comparator<Task> getComparator(SortBy sortBy){
        @NotNull final Comparator<Task> comparator = new Comparator<Task>() {
            @Override
            public int compare(Task task1, Task task2) {

                if (Objects.isNull(task1.getDateStart()) || Objects.isNull(task2.getDateStart())
                        || Objects.isNull(task1.getDateFinish())||Objects.isNull(task2.getDateFinish())
                        ||Objects.isNull(task1.getStatus())||Objects.isNull(task2.getStatus()))
                    return 0;

                if (sortBy.equals(SortBy.ByDateStart)){
                    return task1.getDateStart().toGregorianCalendar().compareTo(task2.getDateStart().toGregorianCalendar());
                }else if (sortBy.equals(SortBy.ByDateFinish)){
                    return task1.getDateFinish().toGregorianCalendar().compareTo(task2.getDateFinish().toGregorianCalendar());
                }else if (sortBy.equals(SortBy.ByStatus)){
                    return task1.getStatus().compareTo(task2.getStatus());
                }
                return 0;
            }
        };
        return comparator;
    }
}
