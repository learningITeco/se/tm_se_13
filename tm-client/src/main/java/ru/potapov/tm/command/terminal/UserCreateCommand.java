package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;

import javax.xml.bind.DatatypeConverter;
import java.lang.Exception;
import java.util.Objects;
import java.util.UUID;
import ru.potapov.tm.endpoint.*;

@Getter
@Setter
@NoArgsConstructor
public final class UserCreateCommand extends AbstractCommand {
    public UserCreateCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        setNeedAuthorize(true);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Creates a new user";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        @Nullable User findUser = null;
        boolean circleForName = true;
        while (circleForName){
            @NotNull String name = getServiceLocator().getTerminalService().readLine("Input a name for a new user:");

            if ("exit".equals(name)){
                return;
            }

            findUser = getServiceLocator().getUserService().getUserByName(name);

            if (Objects.nonNull(findUser)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("User with the name [" + name + "] already exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
            getServiceLocator().getTerminalService().printMassageOk();
            findUser = new User();
            findUser.setLogin(name);
        }

        boolean circleForRole = true;
        while (circleForRole){
            @NotNull String role = getServiceLocator().getTerminalService().readLine("Input a role for this user:");

            if ("exit".equals(role)){
                return;
            }

            @Nullable RoleType roleType = RoleType.valueOf(role);
            if ( Objects.isNull(roleType) ){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("No sach role [" + role + "] exist, plz try again or type command <exit>");
                continue;
            }

            circleForRole = false;
            getServiceLocator().getTerminalService().printMassageOk();
            findUser.setRoleType(roleType);
        }
        findUser.setId(UUID.randomUUID().toString());

        //Pass
        @NotNull String pass = getServiceLocator().getTerminalService().readLine("Input a password for this user:");
        findUser.setHashPass(DatatypeConverter.printHexBinary( getServiceLocator().getUserService().getMd().digest(pass.getBytes()) ) );

        getServiceLocator().getUserService().put(findUser);
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
