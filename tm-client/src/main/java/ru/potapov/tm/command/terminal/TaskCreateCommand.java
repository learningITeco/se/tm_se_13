package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;

import java.lang.Exception;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import ru.potapov.tm.endpoint.*;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {
    public TaskCreateCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        setNeedAuthorize(true);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Creates a new task";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        @Nullable Project findProject = null;
        @NotNull Task newTask = new Task();

        boolean circleForProject = true;
        while (circleForProject) {
            @NotNull String name = getServiceLocator().getTerminalService().readLine("Input the name of the project for this task:");

            if ("exit".equals(name)){
                return;
            }

            findProject = getServiceLocator().getProjectService().findProjectByName(name);

            if (Objects.isNull(findProject)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with the name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            if (!getServiceLocator().getUserService().getAuthorizedUser().getId().equals(findProject.getUserId())){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not belong to you!, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
            newTask.setProjectId(findProject.getId());
            getServiceLocator().getTerminalService().printlnArbitraryMassage("OK");
        }

        boolean circleForName = true;
        while (circleForName){
            @NotNull String name = getServiceLocator().getTerminalService().readLine("Input a name for a new task:");

            if ("exit".equals(name)){
                return;
            }

            boolean exist = false;

            @Nullable Task findTask = getServiceLocator().getTaskService().findTaskByName(name);
            if (Objects.nonNull(findTask)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Task with the name [" + name + "] already exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
            getServiceLocator().getTerminalService().printlnArbitraryMassage("OK");
            newTask.setName(name);
        }

        @NotNull String description = getServiceLocator().getTerminalService().readLine("Input a description for this task:");
        newTask.setDescription(description);

        //Date:
        @NotNull Date date = getServiceLocator().getTerminalService().inputDate("Input a start date for new task in format<dd-mm-yyyy>:");
        newTask.setDateStart(getServiceLocator().getTerminalService().DateToXml(date));

        date = getServiceLocator().getTerminalService().inputDate("Input a finish date for new task:");
        newTask.setDateFinish(getServiceLocator().getTerminalService().DateToXml(date));

        newTask.setId(UUID.randomUUID().toString());
        newTask.setUserId(getServiceLocator().getUserService().getAuthorizedUser().getId());
        newTask.setStatus(Status.PLANNED);

        getServiceLocator().getTaskService().put(newTask);
        getServiceLocator().getTerminalService().printlnArbitraryMassage(getServiceLocator().getTaskService().collectTaskInfo(newTask));
    }
}
