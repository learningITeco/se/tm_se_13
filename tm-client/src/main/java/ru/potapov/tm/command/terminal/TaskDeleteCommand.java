package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;

import java.lang.Exception;
import java.util.Objects;
import ru.potapov.tm.endpoint.*;

@Getter
@Setter
@NoArgsConstructor
public final class TaskDeleteCommand extends AbstractCommand {
    public TaskDeleteCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
        setNeedAuthorize(true);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-delete";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Deletes a task of a project";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        if (!getServiceLocator().getUserService().isAuthorized()){
            getServiceLocator().getTerminalService().printMassageNotAuthorized();
            return;
        }

        if (getServiceLocator().getTaskService().checkSize() == 0){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("We do not have any task.");
            return;
        }

        @Nullable Task findTask = null;

        boolean circleForName = true;
        while (circleForName){
            @NotNull String name = getServiceLocator().getTerminalService().readLine("Input a task name for remove:");

            if ("exit".equals(name)){
                return;
            }

            findTask = getServiceLocator().getTaskService().findTaskByName(name);

            if (Objects.isNull(findTask)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Task with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            if (!getServiceLocator().getUserService().getAuthorizedUser().getId().equals(findTask.getUserId())){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Tsk with name [" + name + "] does not belong to you!, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
        }

        getServiceLocator().getTaskService().remove(findTask);
        getServiceLocator().getTerminalService().printlnArbitraryMassage("The task has deleted");
    }
}
