package ru.potapov.tm.command.terminal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.AbstractCommand;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class UserExitCommand extends AbstractCommand {
    public UserExitCommand(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @NotNull
    @Override
    public String getName() {
        return "user-exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Ends user sassion";
    }

    @Override
    public void execute(@Nullable final String ... additionalCommands) throws Exception {
        if (!super.allowedRun())
            return;
        if (Objects.isNull(getServiceLocator()))
            return;

        getServiceLocator().getSessionService().removeSession(  getServiceLocator().getUserService().getAuthorizedUser().getSession() );
        getServiceLocator().getUserService().setAuthorized(false);
        getServiceLocator().getUserService().setAuthorizedUser(null);
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
