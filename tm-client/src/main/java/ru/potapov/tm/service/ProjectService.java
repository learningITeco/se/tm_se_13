package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.potapov.tm.endpoint.*;
import ru.potapov.tm.api.IProjectService;

import ru.potapov.tm.api.ServiceLocator;

import javax.xml.namespace.QName;
import java.lang.Exception;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectService extends AbstractService<Project> implements IProjectService {
    @Nullable private IProjectEndpoint webService;

    public ProjectService(@NotNull ServiceLocator serviceLocator) throws MalformedURLException {
        super(serviceLocator);
        setUrl(new URL("http://localhost:8080/ProjectEndpoint?wsdl"));
        setQName(new QName("http://endpoint.tm.potapov.ru/", "ProjectEndpointService"));
        setService(getService().create(getUrl(), getQName()));
        setWebService(getService().getPort(IProjectEndpoint.class));
    }

//    @Override
//    public @NotNull Map<String, Project> getMapRepository() {
//        return (Map<String, Project>) webService.getProjectMapRepository().getMap();
//    }

    public int checkSize()  throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return 0;
        return webService.checkProjectSize(getServiceLocator().getSessionService().getSession());
    }

    @Override
    public @Nullable Project findOne(@NotNull String name)  throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return null;
        return webService.findOneProject(getServiceLocator().getSessionService().getSession(), name);
    }

    @Override
    public @Nullable Project findOneById(@NotNull String id) throws ValidateExeption_Exception {
        if (Objects.isNull(webService))
            return null;
        return webService.findOneProjectById(getServiceLocator().getSessionService().getSession(), id);
    }

    //Depricated
    @Override
    public @Nullable Project findOneById(@NotNull String userId, @NotNull String id) throws ValidateExeption_Exception {
        if (Objects.isNull(webService))
            return null;
        return webService.findOneProjectByNameAndUserId(getServiceLocator().getSessionService().getSession(), userId, id);
    }

    @Nullable
    @Override
    public Project findProjectByName(@NotNull final String name) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return null;
        return webService.findOneProject(getServiceLocator().getSessionService().getSession(), name);
    }

    @Nullable
    @Override
    public Project findProjectByName(@NotNull final String userId, @NotNull final String name) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return null;
        return webService.findOneProjectByNameAndUserId(getServiceLocator().getSessionService().getSession(), userId, name);
    }

    @NotNull
    @Override
    public Collection<Project> getCollection(@NotNull final String userId) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return new ArrayList<>();

        return webService.getProjectCollection(getServiceLocator().getSessionService().getSession(), userId);
    }

    @NotNull
    @Override
    public Project renameProject(@NotNull final Project project, @Nullable final String name) throws CloneNotSupportedException_Exception, ValidateExeption_Exception {
        if (Objects.nonNull(name) && Objects.nonNull(webService)){
            return webService.renameProject(getServiceLocator().getSessionService().getSession(), project,name);
        }
        return project;
    }

    @Override
    public void removeAll(@NotNull final String userId) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return;
        webService.removeAllProjectByUserId(getServiceLocator().getSessionService().getSession(), userId);
    }

    @Override
    public void removeAll(@NotNull final Collection<Project> listProjects) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return;
        webService.removeAllProject(getServiceLocator().getSessionService().getSession(), (List<Project>) listProjects);
    }

    @Override
    public void remove(@NotNull final Project project) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return;
        webService.removeProject(getServiceLocator().getSessionService().getSession(), project);
    }

    @Override
    public void put(@NotNull final Project project) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return;
        webService.putProject(getServiceLocator().getSessionService().getSession(), project);
    }

//    @Override
//    public void setMapRepository(@NotNull final Map<String, Project> mapRepository){
//        MyMap myMap = new MyMap();
//        myMap.setMap( mapRepository);
//        webService.setProjectMapRepository(myMap);
//    };

    @Override
    public void loadBinar(@Nullable Session session) throws Exception {
        webService.loadBinar(session);
    }

    @Override
    public void saveBinar(@Nullable Session session) throws Exception{
        webService.saveBinar(session);
    }

    @Override
    public void saveJaxb(@Nullable Session session, boolean formatXml) throws Exception {
        webService.saveJaxb(session, formatXml);
    }

    @Override
    public void loadJaxb(@Nullable Session session, boolean formatXml) throws Exception {
        webService.loadJaxb(session, formatXml);
    }

    @Override
    public void saveFasterXml(@Nullable Session session) throws Exception {
        webService.saveFasterXml(session);
    }

    @Override
    public void loadFasterXml(@Nullable Session session) throws Exception {
        webService.loadFasterXml(session);
    }

    @Override
    public void saveFasterJson(@Nullable Session session) throws Exception {
        webService.saveFasterJson(session);
    }

    @Override
    public void loadFasterJson(@Nullable Session session) throws Exception {
        webService.loadFasterJson(session);
    }

    @NotNull
    @Override
    public String collectProjectInfo(@NotNull final Project project, @NotNull final String owener) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return "";

        return webService.collectProjectInfo(getServiceLocator().getSessionService().getSession(), project, owener);
    }
}
