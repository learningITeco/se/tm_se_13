package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.ServiceLocator;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import ru.potapov.tm.endpoint.*;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractService<T extends Entity> {
    @Nullable private ServiceLocator serviceLocator;

    @Nullable private URL           url;
    @Nullable private QName         qName;
    @Nullable private Service       service;


    public AbstractService(@NotNull ServiceLocator serviceLocator) throws MalformedURLException {
        this.serviceLocator = serviceLocator;
    }
}
