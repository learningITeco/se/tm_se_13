package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.endpoint.*;

import javax.xml.namespace.QName;
import java.lang.Exception;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {
    @Nullable private IUserEndpoint webService;
    @Nullable private MessageDigest md     = null;
    @Nullable private User authorizedUser  = null;
    private boolean isAuthorized           = false;

    @Override
    public @Nullable RoleType getUserRole(@NotNull Session session, @NotNull User user)  throws ValidateExeption_Exception {
        return webService.getUserRole(session, user);
    }

    @Override
    public boolean isAdministrator(@NotNull User user) throws ValidateExeption_Exception{
        @Nullable final Session session = getServiceLocator().getSessionService().getSession();
        if (webService.isAdministrator(session, user))
            return true;

        //getServiceLocator().getTerminalService().printlnArbitraryMassage("Need Administrator property!");
        return false;
    }

    public UserService(@NotNull ServiceLocator serviceLocator) throws MalformedURLException {
        super(serviceLocator);
        setUrl(new URL("http://localhost:8080/UserEndpoint?wsdl"));
        setQName(new QName("http://endpoint.tm.potapov.ru/", "UserEndpointService"));
        setService(getService().create(getUrl(), getQName()));
        setWebService(getService().getPort(IUserEndpoint.class));

        //digest (MD5):
        try {
            md = MessageDigest.getInstance("MD5");
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
//        //Delete
//        User user = getUserByName("user");
//        setAuthorizedUser(user);
//        setAuthorized(true);
    }

    @Override
    public void saveAllUsers() throws Exception {
        if (Objects.isNull(webService))
            return;

        webService.saveAllUsers();
    }

    @Override
    public int size() {
        return getCollectionUser().size();
    }

    @Override
    public void loadAllUsers() throws Exception{
        if (Objects.isNull(webService))
            return;

        webService.saveAllUsers();
    }

    @Override
    public @Nullable User getUserByNamePass(@NotNull String name, @NotNull String pass) throws Exception_Exception {
        if (Objects.isNull(webService))
            return null;

        return webService.getUserByNamePass(name, pass);
    }

    @Nullable
    @Override
    public User create(@Nullable final String name, @Nullable final String hashPass, @Nullable final RoleType role){
        if (Objects.isNull(webService))
            return null;

        return webService.createUser(name, hashPass,role);
    }

    @Nullable
    @Override
    public User getUserByName(@Nullable final String name){
        if (Objects.isNull(webService))
            return null;

        return webService.getUserByName(name);
    }

    @Nullable
    @Override
    public User getUserById(@Nullable final String id)  throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return null;

        return webService.getUserById(getServiceLocator().getSessionService().getSession(), id);
    }

    @NotNull
    @Override
    public Collection<User> getCollectionUser(){
        if (Objects.isNull(webService))
            return new ArrayList<>();

        return webService.getUserCollection();
    }

    @NotNull
    @Override
    public User changePass(@Nullable final User user, @Nullable final String newHashPass) throws CloneNotSupportedException_Exception, ValidateExeption_Exception {
        if (Objects.isNull(webService))
            return user;

        return webService.changePass(getServiceLocator().getSessionService().getSession(), user,newHashPass);
    }

    @Override
    public void put(@Nullable final User user)  throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return;

        webService.putUser(getServiceLocator().getSessionService().getSession(), user) ;
    }

    @NotNull
    @Override
    public String collectUserInfo(@Nullable final User user) throws ValidateExeption_Exception{
        if (Objects.isNull(webService))
            return "";

        return webService.collectUserInfo(getServiceLocator().getSessionService().getSession(), user);
    }
}
