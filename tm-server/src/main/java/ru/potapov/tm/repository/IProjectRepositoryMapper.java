package ru.potapov.tm.repository;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.jetbrains.annotations.NotNull;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Project;

import java.util.Collection;
import java.util.List;


public interface IProjectRepositoryMapper {
    @NotNull final String findOneById = "SELECT * FROM app_project WHERE id = #{id}  LIMIT 1";
    @NotNull final String findAll = "SELECT * FROM app_project";
    @NotNull final String findAllByUserId = "SELECT * FROM app_project WHERE user_id = @{userId}";
    @NotNull final String findOneByName = "SELECT * FROM app_project WHERE name = #{name}  LIMIT 1";
    @NotNull final String FindOneByNameByUserId = "SELECT * FROM app_project WHERE name = #{name} AND user_id = #{id}  LIMIT 1";
    @NotNull final String persist = "INSERT INTO app_project (name, description, user_id, dateBegin, dateEnd, id)  VALUES (#{name}, #{description}, #{userId}, #{dateStart}, #{dateFinish}, #{id}) ";
    @NotNull final String merge = "UPDATE app_project SET name=#{name}, description=#{description}, user_id=#{userId}, dateBegin=#{dateStart}, dateEnd=#{dateFinish} WHERE id = #{id}";
    @NotNull final String removeById = "DELETE FROM app_project WHERE  id = #{id} ";
    @NotNull final String removeAll = "DELETE * FROM app_project";

    @Select(findOneById)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    @Nullable Project findOneById(@NotNull final String id);

    @Select(findAll)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    @NotNull Collection<Project> findAll();

    @Select(findAllByUserId)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    @NotNull Collection<Project> findAllByUserId(@NotNull final String userId);

    @Select(findOneByName)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    @Nullable Project findOne(@NotNull final String name);

    @Select(FindOneByNameByUserId)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    @Nullable Project FindOneByNameByUserId(@NotNull final String userId, @NotNull final String name);

    @Insert(persist)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    void persist(@NotNull final Project project);

    @Update(merge)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    void merge(@NotNull final Project project);

    @Delete(removeById)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    void remove(@NotNull final String id) ;

    @Delete(removeAll)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    void removeAll();
}
