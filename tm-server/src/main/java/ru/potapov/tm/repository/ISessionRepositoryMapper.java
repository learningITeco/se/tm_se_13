package ru.potapov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Session;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface ISessionRepositoryMapper {
    
    @NotNull final String findAll = "SELECT * FROM app_session";
    @NotNull final String findOne = "SELECT * FROM app_session WHERE id = #{id}  LIMIT 1";
    @NotNull final String persist = "INSERT INTO app_session(id, user_id, signature, timestamp) VALUES (#{id}, #{userId}, #{signature}, #{dateStamp})";
    @NotNull final String merge = "UPDATE app_session SET signature = #{signature}, timestamp = #{dateStamp}, user_id = #{userId} WHERE id = #{id}";
    @NotNull final String remove = "DELETE FROM app_session WHERE  id = #{id} ";
    @NotNull final String removeAll = "DELETE * FROM app_session";

    @Select(findAll)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStamp", column = "timestamp")})
    @NotNull Collection<Session> findAll();

    @Select(findOne)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStamp", column = "timestamp")})
    @Nullable Session findOne(@NotNull final String id);

    @Insert(persist)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "dateStamp", column = "timestamp")})
    void persist(@NotNull final Session session);

    @Update(merge)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "signature", column = "signature"),
            @Result(property = "dateStamp", column = "timestamp")})
    void merge(@NotNull final Session session);

    @Delete(remove)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStamp", column = "timestamp")})
    void remove(@NotNull final String id) ;

    @Delete(removeAll)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStamp", column = "timestamp")})
    void removeAll();

}
