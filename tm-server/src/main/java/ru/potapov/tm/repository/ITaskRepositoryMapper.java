package ru.potapov.tm.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Task;

import java.util.Collection;

public interface ITaskRepositoryMapper {
    @NotNull final String findAll = "SELECT * FROM app_task";
    @NotNull final String findAllByUserId = "SELECT * FROM app_task WHERE user_id = #{userId}";
    @NotNull final String findAllByUserIdAndProjectId = "SELECT * FROM app_task WHERE user_id = #{userId} AND project_id = #{projectId}";
    @NotNull final String findOneById = "SELECT * FROM app_task WHERE id = #{id}  LIMIT 1";
    @NotNull final String findOne = "SELECT * FROM app_task WHERE name = #{name}  LIMIT 1";
    @Nullable final String findOneByNameAndUserId = "SELECT * FROM app_task WHERE name = #{name} AND user_id = #{userId}  LIMIT 1";
    @NotNull final String persist = "INSERT INTO app_task (name, description, user_id, dateBegin, dateEnd, project_id, id)  VALUES (#{name}, #{description}, #{userId}, #{dateStart}, #{dateFinish}, #{projectId}, #{id}) ";
    @NotNull final String merge = "UPDATE app_task SET name=#{name}, description=#{description}, user_id=#{userId}, dateBegin=#{dateStart}, dateEnd=#{dateFinish} , project_id = #{projectId} WHERE id = #{id}";
    @NotNull final String remove = "DELETE FROM app_task WHERE  id = #{id} ";
    @NotNull final String removeAll = "DELETE * FROM app_task";

    @Select(findAll)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    @Nullable Collection<Task> findAll();

    @Select(findAllByUserId)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    @NotNull  Collection<Task> findAllByUser(@NotNull final String userId);

    @Select(findAllByUserIdAndProjectId)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    @Nullable  Collection<Task> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);
    
    @Select(findOneById)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    @Nullable Task findOneById(@NotNull String id);

    @Select(findOne)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    @Nullable Task findOne(@NotNull String id);

    @Select(findOneByNameAndUserId)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    @Nullable Task findOneByNameAndUserId(@NotNull final String userId, @NotNull final String name) ;

    @Insert(persist)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    void  persist(@NotNull final Task task) ;

    @Update(merge)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    void merge(@NotNull final Task task);

    @Delete(remove)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    void remove(@NotNull final String id);

    @Delete(removeAll)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd")})
    void removeAll();
}
