package ru.potapov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;

public interface IUserRepositoryMapper {
    @NotNull final String findOne = "SELECT * FROM app_user WHERE login = #{login} LIMIT 1";
    @NotNull final String findOneById = "SELECT * FROM app_user WHERE id = #{id}  LIMIT 1";
    @NotNull final String findAll = "SELECT * FROM app_user";
    @NotNull final String persist = "INSERT INTO app_user  (login, passwordHash, role, id) VALUES (#{login}, #{hashPass}, #{roleType}, #{id})";
    @NotNull final String merge = "UPDATE app_user SET login=#{login}, passwordHash=#{hashPass}, role=#{roleType} WHERE id = #{id}";
    @NotNull final String remove = "DELETE FROM app_user WHERE  id = #{id} ";
    @NotNull final String removeAll = "DELETE * FROM app_user";

    @Select(findOne)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "roleType", column = "role"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "hashPass", column = "passwordHash")})
    @Nullable User findOne(@NotNull final String login);

    @Select(findOneById)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "roleType", column = "role"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "hashPass", column = "passwordHash")})
    @Nullable User findOneById(@NotNull final String id);

    @Select(findAll)
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "roleType", column = "role"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "hashPass", column = "passwordHash")})
    @NotNull Collection<User> findAll();

    @Insert(persist)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "roleType", column = "role"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "hashPass", column = "passwordHash")})
    void persist(@NotNull final String id);

    @Update(merge)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "roleType", column = "role"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "hashPass", column = "passwordHash")})
    void merge(@NotNull final String id);

    @Delete(remove)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "roleType", column = "role"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "hashPass", column = "passwordHash")})
    void remove(@NotNull final String id) ;

    @Delete(removeAll)
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "roleType", column = "role"),
            @Result(property = "dateStart", column = "dateBegin"),
            @Result(property = "dateFinish", column = "dateEnd"),
            @Result(property = "hashPass", column = "passwordHash")})
    void removeAll() ;
}
