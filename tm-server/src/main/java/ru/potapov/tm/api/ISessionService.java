package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface ISessionService {
    boolean validSession(@NotNull Session session) throws ValidateExeption;
    @Nullable Session generateSession(@NotNull Session session, @NotNull final User user);
    @Nullable Collection<Session> getSessionCollection();
    void addSession(@NotNull Session session, String userId) throws ValidateExeption;
    void removeSession(@NotNull Session session) throws ValidateExeption;
}
