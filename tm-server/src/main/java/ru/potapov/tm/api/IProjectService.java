package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IProjectService {
    int checkProjectSize() throws ValidateExeption;
    @Nullable Project findOneProject(@NotNull final String name) throws ValidateExeption;
    @Nullable Project findProjectByName(@NotNull final String name) throws ValidateExeption;
    @Nullable Project findProjectByNameAndUserId(@NotNull final String userId, @NotNull final String name) throws ValidateExeption;
    @Nullable Project findOneProjectById(@NotNull final String id) throws ValidateExeption;
    @Nullable Project findOneProjectByNameAndUserId(@NotNull final String userId, @NotNull final String id) throws ValidateExeption;
    @NotNull Collection<Project> getProjectCollection(@NotNull final String userId) throws ValidateExeption;
    @NotNull Project renameProject(@NotNull final Project project, @NotNull final String name) throws CloneNotSupportedException, ValidateExeption;
    void removeAllProjectByUserId(@NotNull final String userId) throws ValidateExeption;
    void removeAllProject(Collection<Project> listProjects) throws ValidateExeption;
    void removeProject(@NotNull final Project project) throws ValidateExeption;
    void putProject(@NotNull final Project project) throws ValidateExeption;
    @NotNull String collectProjectInfo(@NotNull final Project project, @NotNull final String owener) throws ValidateExeption;
    void loadBinar() throws Exception;
    void saveBinar() throws Exception;

    void saveJaxb(final boolean formatXml) throws Exception;
    void loadJaxb(final boolean formatXml) throws Exception;

    void saveFasterXml() throws Exception;
    void loadFasterXml() throws Exception;

    void saveFasterJson() throws Exception;
    void loadFasterJson() throws Exception;
}
