package ru.potapov.tm.api;


import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.entity.Task;

import java.util.Collection;

public interface ITaskRepository extends IRepository<Task> {
    @NotNull Collection findAll(@NotNull final String userId, @NotNull final String projectId);
    @NotNull Collection findAllByUser(@NotNull final String userI);
}
