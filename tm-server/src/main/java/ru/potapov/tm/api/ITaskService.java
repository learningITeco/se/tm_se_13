package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface ITaskService {
    int checkTaskSize() throws ValidateExeption;
    @Nullable Task findTaskByName(@NotNull final String name) throws ValidateExeption;
    void removeTask(@NotNull final Task task) throws ValidateExeption;
    void removeAllTasksByUserId(@NotNull final String userId) throws ValidateExeption;
    void removeAllTasks(@NotNull final Collection<Task> listTasks) throws ValidateExeption;
    @NotNull Task renameTask(@NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException, ValidateExeption;
    void changeProject(@NotNull final Task task, @NotNull final Project project) throws CloneNotSupportedException, ValidateExeption;
    @NotNull Collection<Task> findAllTasks(@NotNull final String projectId) throws ValidateExeption;
    @NotNull Collection<Task> findAllTasksByUserId(@NotNull final String userId, @NotNull final String projectId) throws ValidateExeption;
    @NotNull Collection<Task> findAllByUser(@NotNull final String userId) throws ValidateExeption;
    void putTask(@NotNull Task task) throws ValidateExeption;
    @NotNull String collectTaskInfo(@NotNull final Task task) throws ValidateExeption;

    void loadBinar() throws Exception;
    void saveBinar() throws Exception;

    void saveJaxb(final boolean formatXml) throws Exception;
    void loadJaxb(final boolean formatXml) throws Exception;

    void saveFasterXml() throws Exception;
    void loadFasterXml() throws Exception;

    void saveFasterJson() throws Exception;
    void loadFasterJson() throws Exception;
}
