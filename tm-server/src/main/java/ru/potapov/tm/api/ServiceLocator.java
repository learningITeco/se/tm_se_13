package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.potapov.tm.endpoint.IProjectEndpoint;
import ru.potapov.tm.endpoint.ISessionEndpoint;
import ru.potapov.tm.endpoint.ITaskEndpoint;
import ru.potapov.tm.endpoint.IUserEndpoint;

import java.text.SimpleDateFormat;

public interface ServiceLocator {
    @NotNull IProjectService getProjectService();
    @NotNull ITaskService getTaskService();
    @NotNull ISessionService getSessionService();
    @NotNull IUserService getUserService();
    @NotNull ITerminalService getTerminalService();
    @NotNull SimpleDateFormat getFt();
}
