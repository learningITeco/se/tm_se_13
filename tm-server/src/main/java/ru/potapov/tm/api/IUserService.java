package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IUserService {
    @NotNull User createUser(@NotNull final String name, @NotNull final String hashPass, @NotNull final RoleType role);
    @Nullable User getUserByName(@NotNull final String name);
    @Nullable User getUserByNamePass(@NotNull final String name, @NotNull final String pass) throws Exception;
    @Nullable User getUserById(@NotNull final String id) throws ValidateExeption;
    boolean isUserPassCorrect(@NotNull final User user, @NotNull final String hashPass);
    @NotNull Collection<User> getUserCollection();
    @NotNull User changePass(@NotNull User user, @NotNull final String newHashPass) throws CloneNotSupportedException, ValidateExeption;
    void putUser(@NotNull final User user) throws ValidateExeption;
    @NotNull String collectUserInfo(@NotNull final User user) throws ValidateExeption;
    void createPredefinedUsers();
    void setAuthorizedUser(@NotNull final User authorizedUser);
    void saveAllUsers() throws Exception;
    void loadAllUsers() throws Exception;
    int sizeUserMap();
    @Nullable RoleType getUserRole(@NotNull final User user) throws ValidateExeption;
    boolean isAdministrator(@NotNull final User user) throws ValidateExeption;
}
