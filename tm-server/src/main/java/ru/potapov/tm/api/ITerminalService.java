package ru.potapov.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ITerminalService {
    void printMassageNotAuthorized();
    void printMassageCompleted();
    void printMassageOk();
    void printlnArbitraryMassage(@NotNull final String msg);
    void printArbitraryMassage(@NotNull final String msg);
}
