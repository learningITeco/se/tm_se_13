package ru.potapov.tm;

import org.jetbrains.annotations.NotNull;

import ru.potapov.tm.bootstrap.Bootstrap;

/**
 * Application
 * v 1.0.7
 */
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class Application {
    @NotNull final private String version = "1.0.7";


    public Application() {
        try {
            @NotNull final Bootstrap bootstrap = new Bootstrap();
            bootstrap.init();
        }catch (Exception e){ e.printStackTrace(); }
    }

    public static void main(String[] args) {
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        new Application();
    }
}
