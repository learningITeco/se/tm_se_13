package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.ISessionService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.repository.ISessionRepositoryMapper;
import ru.potapov.tm.util.JavaToMysql;
import ru.potapov.tm.util.SignatureUtil;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.potapov.tm.endpoint.ISessionEndpoint")
public class SessionService extends AbstractService<Session> implements ISessionService {
    @Nullable ISessionRepositoryMapper mapper = null;
    @NotNull final Map<String, Session> mapSession = new HashMap<>();
    @Nullable private Bootstrap bootstrap;

    public SessionService(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.bootstrap = bootstrap;
    }

    @Override
    public boolean validSession(@NotNull Session sessionUser) throws ValidateExeption {
        @Nullable Session sessionUserClone;
        try { sessionUserClone = sessionUser.clone(); }catch (Exception e){return false;}
        sessionUserClone.setSignature("");
        @Nullable final String sign = SignatureUtil.sign(sessionUserClone,"",1);
        if ( sign.equals(sessionUser.getSignature()) ){
            final long timDif = (new Date().getTime() - sessionUser.getDateStamp());
            if ( timDif < 86400000 )
                return true;
        }
        return false;
    }

    @Override
    public @Nullable Session generateSession(@NotNull Session sessionUser, @NotNull User user) {
        @Nullable Session sessionUserClone;
        try { sessionUserClone = sessionUser.clone(); }catch (Exception e){return null;}
        sessionUserClone.setSignature("");
        sessionUser.setSignature(SignatureUtil.sign(sessionUserClone,"", 1));
        sessionUser.setUserId(user.getId());
        return sessionUser;
    }

    @Override
    public void addSession(Session sessionUser, String userId) throws ValidateExeption {
        getServiceLocator().getSessionService().validSession(sessionUser);
        try {
            session = getJavaToMysql().getSqlSession(ISessionRepositoryMapper.class);
            mapper = session.getMapper(ISessionRepositoryMapper.class);
            if (userId.equals(sessionUser.getUserId()))
                mapper.persist(sessionUser);
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
    }

    @Override
    public @Nullable Collection<Session> getSessionCollection() {
        @Nullable Collection<Session> list = new ArrayList<>();
        try {
            session = getJavaToMysql().getSqlSession(ISessionRepositoryMapper.class);
            mapper = session.getMapper(ISessionRepositoryMapper.class);
            list = mapper.findAll();
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return list;
    }

    @Override
    public void removeSession(@NotNull Session sessionUser) {
        try {
            session = getJavaToMysql().getSqlSession(ISessionRepositoryMapper.class);
            mapper = session.getMapper(ISessionRepositoryMapper.class);
            mapper.remove(sessionUser.getId());
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
    }
}
