package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.repository.IUserRepositoryMapper;
import ru.potapov.tm.util.JavaToMysql;
import ru.potapov.tm.util.SignatureUtil;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import java.io.*;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.potapov.tm.endpoint.IUserEndpoint")
public final class UserService extends AbstractService<User> implements IUserService {
    @Nullable IUserRepositoryMapper mapper = null;
    @Nullable
    private User authorizedUser = null;
    private boolean isAuthorized = false;

    public UserService(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @Nullable RoleType getUserRole(@NotNull User user) throws ValidateExeption {
        @Nullable RoleType roleType = RoleType.User;
        try {
            session = getJavaToMysql().getSqlSession(IUserRepositoryMapper.class);
            mapper = session.getMapper(IUserRepositoryMapper.class);            
            roleType = user.getRoleType();
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return roleType;
    }

    @Override
    public boolean isAdministrator(@NotNull User user) throws ValidateExeption {
        if (getUserRole(user).equals(RoleType.Administrator))
            return true;

        return false;
    }

    @Override
    public @Nullable User getUserByNamePass(@NotNull final String name, @NotNull final String pass) throws Exception {
        @NotNull User user = getUserByName(name);
        if (Objects.isNull(user)) {
            return null;
        }

        @NotNull final String hashPass = SignatureUtil.sign(pass, "", 1);

        if (!isUserPassCorrect(user, hashPass)) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage("Failed the user password!");
            return null;
        }

        @NotNull Session session = new Session();
        session.setUserId(user.getId());
        getServiceLocator().getSessionService().generateSession(session, user);
        getServiceLocator().getSessionService().addSession(session, user.getId());
        user.setSession(session);
        return user;
    }

    @Override
    public int sizeUserMap() {
        return getUserCollection().size();
    }

    @Override
    public void saveAllUsers() throws Exception {
//        @NotNull final File file = new File("users.bin");
//        @NotNull final ObjectOutputStream inputStream = new ObjectOutputStream(new FileOutputStream(file));
//        @NotNull Map<String, User> mapObj = new HashMap<>();
//        try {
//            session = getJavaToMysql().getSqlSession(IUserRepositoryMapper.class);
//            mapper = session.getMapper(IUserRepositoryMapper.class);
//            mapObj = (Map<String, User>) getUserMapRepository().getMap();
//            session.commit();
//        }catch (Exception e){e.printStackTrace(); session.rollback();}
//        finally {
//            session.close();}
//        inputStream.writeObject(mapObj);
    }

    @Override
    public void loadAllUsers() throws Exception {
//        @NotNull final File file = new File("users.bin");
//        if (!file.canRead()) {
//            //getBootstrap().getTerminalService().printlnArbitraryMassage("File cannot be opened");
//            return;
//        }
//
//        @NotNull final ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file));
//        @NotNull final Object objMapUser = inputStream.readObject();
//        if (objMapUser instanceof Map) {
//            @NotNull final Map<String, User> mapUser = (Map<String, User>) objMapUser;
//            try {
//                session = getJavaToMysql().getSqlSession(IUserRepositoryMapper.class);
//            mapper = session.getMapper(IUserRepositoryMapper.class);
//                setUserMapRepository(new MyMap(mapUser));
//                session.commit();
//            }catch (Exception e){e.printStackTrace(); session.rollback();}
//            finally {
//                session.close();}
//        }
    }

    @NotNull
    @Override
    public User createUser(@Nullable final String name, @Nullable final String hashPass, @Nullable final RoleType role) {
        @NotNull User user = new User();
        user.setRoleType(role);
        user.setLogin(name);
        user.setHashPass(hashPass);
        user.setId(UUID.randomUUID().toString());

        try {
            session = getJavaToMysql().getSqlSession(IUserRepositoryMapper.class);
            mapper = session.getMapper(IUserRepositoryMapper.class);
            mapper.persist(user.getId());
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}

        return user;
    }

    @Nullable
    @Override
    public User getUserByName(@Nullable final String name) {
        @Nullable User user = null;
        try {
            session = getJavaToMysql().getSqlSession(IUserRepositoryMapper.class);
            mapper = session.getMapper(IUserRepositoryMapper.class);
            user = mapper.findOne(name);
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}

        return user;
    }

    @Nullable
    @Override
    public User getUserById(@Nullable final String id) throws ValidateExeption {
        @Nullable User user = null;
        try {
            session = getJavaToMysql().getSqlSession(IUserRepositoryMapper.class);
            mapper = session.getMapper(IUserRepositoryMapper.class);
            user =  mapper.findOneById(id);
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}

        return user;
    }

    @Override
    public boolean isUserPassCorrect(@Nullable final User user, @Nullable final String hashPass) {
        boolean res = false;
        if (user.getHashPass().equals(hashPass))
            res = true;

        return res;
    }

    @NotNull
    @Override
    public Collection<User> getUserCollection() {
        @NotNull Collection<User> list = new ArrayList<>();
        try {
            session = getJavaToMysql().getSqlSession(IUserRepositoryMapper.class);
            mapper = session.getMapper(IUserRepositoryMapper.class);
            list = mapper.findAll();
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return list;
    }

    @NotNull
    @Override
    public User changePass(@Nullable User user, @Nullable final String newHashPass) throws CloneNotSupportedException, ValidateExeption {
        @NotNull User newUser = (User) user.clone();
        newUser.setHashPass(newHashPass);
        try {
            session = getJavaToMysql().getSqlSession(IUserRepositoryMapper.class);
            mapper = session.getMapper(IUserRepositoryMapper.class);
            mapper.merge(newUser.getId());
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return user;
    }

    @Override
    public void putUser(@Nullable final User user) throws ValidateExeption {
        try {
            session = getJavaToMysql().getSqlSession(IUserRepositoryMapper.class);
            mapper = session.getMapper(IUserRepositoryMapper.class);
            mapper.persist(user.getId());
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
    }

    @NotNull
    @Override
    public String collectUserInfo(@Nullable final User user) throws ValidateExeption {
        @NotNull String res = "";
        res += "\n";
        res += "    User [" + user.getLogin() + "]" + "\n";
        res += "    ID: " + user.getId() + "\n";
        res += "    Role: " + user.getRoleType() + "\n";

        return res;
    }

    @Override
    public void createPredefinedUsers() {
        @NotNull String hashPass;

        hashPass = SignatureUtil.sign("1", "", 1);
        createUser("user", hashPass, RoleType.User);

        hashPass = SignatureUtil.sign("1", "", 1);
        createUser("user2", hashPass, RoleType.User);

        hashPass = SignatureUtil.sign("2", "", 1);
        createUser("admin", hashPass, RoleType.Administrator);
    }
}
