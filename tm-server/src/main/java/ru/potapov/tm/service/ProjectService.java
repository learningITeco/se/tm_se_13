package ru.potapov.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IProjectService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.DataXml;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.repository.IProjectRepositoryMapper;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.*;

@WebService(endpointInterface = "ru.potapov.tm.endpoint.IProjectEndpoint")
public final class ProjectService extends AbstractService<Project> implements IProjectService {
    @Nullable IProjectRepositoryMapper mapper = null;
    public ProjectService(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public int checkProjectSize() throws ValidateExeption {
        int i = 0;
        try {
            session = getJavaToMysql().getSqlSession(IProjectRepositoryMapper.class);
            mapper = session.getMapper(IProjectRepositoryMapper.class);
            i = mapper.findAll().size();
            session.commit();            
            }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return i;
    }

    @Override
    public @Nullable Project findOneProject(@NotNull String name) throws ValidateExeption{
        @Nullable Project project = null;
        try {
            session = getJavaToMysql().getSqlSession(IProjectRepositoryMapper.class);
            mapper = session.getMapper(IProjectRepositoryMapper.class);
            project = mapper.findOne(name);
             session.commit();
            }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}

        return project;
    }

    @Override
    public @Nullable Project findOneProjectById(@NotNull String id) throws ValidateExeption{
        @Nullable Project project = null;
        try {
            session = getJavaToMysql().getSqlSession(IProjectRepositoryMapper.class);
            mapper = session.getMapper(IProjectRepositoryMapper.class);
            project = mapper.findOneById(id);
            session.commit();
            }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return project;
    }

    @Override
    public @Nullable Project findOneProjectByNameAndUserId(@NotNull String userId, @NotNull String name) throws ValidateExeption{
        @Nullable Project project = null;
        try {
            session = getJavaToMysql().getSqlSession(IProjectRepositoryMapper.class);
            mapper = session.getMapper(IProjectRepositoryMapper.class);
            project = mapper.FindOneByNameByUserId(userId, name);
            session.commit();
            }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return project;
    }

    @Nullable
    @Override
    public Project findProjectByName(@NotNull final String name) throws ValidateExeption{
        @Nullable Project project = null;
        try {
            session = getJavaToMysql().getSqlSession(IProjectRepositoryMapper.class);
            mapper = session.getMapper(IProjectRepositoryMapper.class);
            project = mapper.findOne(name);
            session.commit();
            }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return project;
    }

    @Nullable
    @Override
    public Project findProjectByNameAndUserId(@NotNull final String userId, @NotNull final String name) throws ValidateExeption{
        @Nullable Project project = null;
        try {
            session = getJavaToMysql().getSqlSession(IProjectRepositoryMapper.class);
            mapper = session.getMapper(IProjectRepositoryMapper.class);
            project = mapper.FindOneByNameByUserId(userId, name);
            session.commit();
            }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> getProjectCollection(@NotNull final String userId) throws ValidateExeption{
        if (Objects.isNull(mapper))
            return new ArrayList<>();

        @Nullable  final User user = getServiceLocator().getUserService().getUserById( userId);
        boolean isAdmin = getServiceLocator().getUserService().isAdministrator( user);
        @Nullable Collection<Project> list = new ArrayList<>();
        try {
            session = getJavaToMysql().getSqlSession(IProjectRepositoryMapper.class);
            mapper = session.getMapper(IProjectRepositoryMapper.class);
            if ( isAdmin )
                list = mapper.findAll();
            else
                list =  mapper.findAllByUserId(userId);
             session.commit();
            }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return list;
    }

    @NotNull
    public Collection<Project> getProjectCollection() throws ValidateExeption{
        if (Objects.isNull(mapper))
            return new ArrayList<>();

        @Nullable Collection<Project> list = new ArrayList<>();
        try {
            session = getJavaToMysql().getSqlSession(IProjectRepositoryMapper.class);
            mapper = session.getMapper(IProjectRepositoryMapper.class);
            list = mapper.findAll();
            session.commit();
            }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return list;
    }

    @NotNull
    @Override
    public Project renameProject(@NotNull final Project project, @Nullable final String name) throws CloneNotSupportedException, ValidateExeption{
        if (Objects.nonNull(name) && Objects.nonNull(mapper) ){
            @Nullable Project newProject = (Project) project.clone();
            newProject.setName(name);
            try {
                session = getJavaToMysql().getSqlSession(IProjectRepositoryMapper.class);
                mapper = session.getMapper(IProjectRepositoryMapper.class);
                mapper.merge(newProject);
                session.commit();
            }catch (Exception e){e.printStackTrace(); session.rollback();}
            finally {
                session.close();}
        }
        return project;
    }

    @Override
    public void removeAllProjectByUserId(@NotNull final String userId) throws ValidateExeption{
        if (Objects.isNull(mapper))
            return;
        try {
            session = getJavaToMysql().getSqlSession(IProjectRepositoryMapper.class);
            mapper = session.getMapper(IProjectRepositoryMapper.class);
            for (Project project : mapper.findAllByUserId(userId)) {
                if (userId.equals(project.getUserId()))
                    mapper.remove(project.getId());
            }
             session.commit();
            }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
    }
    
    @Override
    public void removeAllProject(@NotNull final Collection<Project> listProjects) throws ValidateExeption{
        if (Objects.isNull(mapper))
            return;
        try {
            session = getJavaToMysql().getSqlSession(IProjectRepositoryMapper.class);
            mapper = session.getMapper(IProjectRepositoryMapper.class);
            for (Project project : listProjects) {
                mapper.remove(project.getId());
            }
             session.commit();
            }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
    }
    
    @Override
    public void removeProject(@NotNull final Project project) throws ValidateExeption{
        if (Objects.isNull(mapper))
            return;
        try {
            session = getJavaToMysql().getSqlSession(IProjectRepositoryMapper.class);
            mapper = session.getMapper(IProjectRepositoryMapper.class);
            mapper.remove(project.getId());
            session.commit();
            }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
    }
    
    @Override
    public void putProject(@NotNull final Project project) throws ValidateExeption{
        if (Objects.isNull(mapper))
            return;

        boolean exist = Objects.nonNull(findOneProjectById( project.getId()));
        try {
            session = getJavaToMysql().getSqlSession(IProjectRepositoryMapper.class);
            mapper = session.getMapper(IProjectRepositoryMapper.class);
            if (exist)
                mapper.merge(project);
            else
                mapper.persist(project);
            session.commit();
            }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
    }

    @NotNull
    @Override
    public String collectProjectInfo(@NotNull final Project project, @NotNull final String owener) throws ValidateExeption{
        @NotNull String res = "";

        if (Objects.isNull(project.getDateStart()) || Objects.isNull(project.getDateFinish()))
            return res;

        res += "\n";
        res += "Project [" + project.getName() + "]" +  "\n";
        res += "Owner: " + owener + "\n";
        res += "Status: " + project.getStatus() + "\n";
        res += "Description: " + project.getDescription() + "\n";
        res += "ID: " + project.getId() + "\n";
        res += "Date start: " + getServiceLocator().getFt().format(project.getDateStart() ) + "\n";
        res += "Date finish: " +  getServiceLocator().getFt().format(project.getDateFinish()) + "\n";

        return res;
    }

    //Save-Load
    @Override
    public void saveBinar() throws Exception{
        @NotNull final File file = new File("data-"+ getClassName() +".binar");
        @NotNull final ObjectOutputStream inputStream = new ObjectOutputStream(new FileOutputStream(file));

        @NotNull DataXml dataXml = new DataXml();
        dataXml.getListValueProject().addAll(getProjectCollection());
        dataXml.getListValueTask().addAll(getServiceLocator().getTaskService().findAllTasks(""));

        inputStream.writeObject( dataXml );
        inputStream.close();
    }

    @Override
    public void loadBinar() throws Exception{
        @NotNull final File file = new File("data-"+ getClassName() +".binar");
        if (!file.canRead()){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("File cannot be opened");
            return;
        }

        @NotNull final ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file));
        @NotNull final Object dataObj = inputStream.readObject();
        if (dataObj instanceof DataXml) {
            @NotNull final DataXml data = (DataXml) dataObj;

            if (data.getListValueProject().size() > 0 ){
                for (int i = 0; i < data.getListValueProject().size(); i++) {
                    putProject(data.getListValueProject().get(i));
                }
            }

            if ( data.getListValueTask().size() > 0){
                for (int i = 0; i < data.getListValueTask().size(); i++) {
                    getServiceLocator().getTaskService().putTask(data.getListValueTask().get(i));
                }
            }
        }
    }
    @Override
    public void saveJaxb(boolean formatXml) throws Exception {
        @NotNull DataXml dataXml = new DataXml();
        dataXml.getListValueProject().addAll(getProjectCollection());
        dataXml.getListValueTask().addAll(getServiceLocator().getTaskService().findAllTasks(""));

        @NotNull final JAXBContext context      = JAXBContext.newInstance(DataXml.class) ;
        @NotNull final Marshaller marshaller    = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        @NotNull String typeFormatName = "xml";
        if (!formatXml){
            typeFormatName = "json";
            marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
            marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        }
        @NotNull final File file = new File("data-jaxb-"+getClassName()+"."+typeFormatName);
        marshaller.marshal(dataXml, new FileWriter(file));
    }
    @Override
    public void loadJaxb(boolean formatXml) throws Exception {
        @NotNull final JAXBContext context = JAXBContext.newInstance(DataXml.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();

        @NotNull String typeFormatName = "xml";
        if (!formatXml) {
            typeFormatName = "json";
            unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
            unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        }

        @NotNull final File file = new File("data-jaxb-"+getClassName()+"."+typeFormatName);
        @NotNull final DataXml data =(DataXml) unmarshaller.unmarshal(file);

        if (data.getListValueProject().size() > 0 ){
            for (int i = 0; i < data.getListValueProject().size(); i++) {
                putProject(data.getListValueProject().get(i));
            }
        }

        if ( data.getListValueTask().size() > 0){
            for (int i = 0; i < data.getListValueTask().size(); i++) {
                getServiceLocator().getTaskService().putTask(data.getListValueTask().get(i));
            }
        }
    }

    @Override
    public void saveFasterXml() throws Exception {
        @NotNull DataXml dataXml = new DataXml();
        dataXml.getListValueProject().addAll(getProjectCollection());
        dataXml.getListValueTask().addAll(getServiceLocator().getTaskService().findAllTasks(""));

        @NotNull final File file = new File("data-faster-"+getClassName()+".xml");
        @NotNull final XmlMapper  mapper = new XmlMapper();
        mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, dataXml);
    }
    @Override
    public void loadFasterXml() throws Exception {
        @NotNull final File file = new File("data-faster-"+getClassName()+".xml");
        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        @NotNull final DataXml data = mapper.readValue(file, DataXml.class);

        @NotNull Map<String, Project> mapProject = new HashMap<>();
        @NotNull Map<String, Task> mapTask       = new HashMap<>();
        if (data.getListValueProject().size() > 0 ){
            for (int i = 0; i < data.getListValueProject().size(); i++) {
                putProject(data.getListValueProject().get(i));
            }
        }

        if ( data.getListValueTask().size() > 0){
            for (int i = 0; i < data.getListValueTask().size(); i++) {
                getServiceLocator().getTaskService().putTask(data.getListValueTask().get(i));
            }
        }
    }

    @Override
    public void saveFasterJson() throws Exception {
        @NotNull DataXml dataXml = new DataXml();
        dataXml.getListValueProject().addAll(getProjectCollection());
        dataXml.getListValueTask().addAll(getServiceLocator().getTaskService().findAllTasks(""));

        @NotNull final File file = new File("data-faster-"+getClassName()+".json");
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String s = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dataXml);
        FileOutputStream outputStream = new FileOutputStream(file);
        outputStream.write(s.getBytes());
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
    @Override
    public void loadFasterJson() throws Exception {
        @NotNull final File file = new File("data-faster-"+getClassName()+".json");
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final DataXml data = mapper.readValue(file, DataXml.class);
        if (data.getListValueProject().size() > 0 ){
            for (int i = 0; i < data.getListValueProject().size(); i++) {
                putProject(data.getListValueProject().get(i));
            }
        }

        if ( data.getListValueTask().size() > 0){
            for (int i = 0; i < data.getListValueTask().size(); i++) {
                getServiceLocator().getTaskService().putTask(data.getListValueTask().get(i));
            }
        }

    }
}
