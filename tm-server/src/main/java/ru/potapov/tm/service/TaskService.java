package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.ITaskRepository;
import ru.potapov.tm.api.ITaskService;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.repository.ITaskRepositoryMapper;
import ru.potapov.tm.util.JavaToMysql;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.potapov.tm.endpoint.ITaskEndpoint")
public final class TaskService extends AbstractService<Task> implements ITaskService {
    @Nullable ITaskRepositoryMapper mapper = null;
    public TaskService(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public int checkTaskSize() throws ValidateExeption{
        int i = 0;
        try {
            session = getJavaToMysql().getSqlSession(ITaskRepositoryMapper.class);
            mapper = session.getMapper(ITaskRepositoryMapper.class);
            i =  mapper.findAll().size();
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return i;
    }

    @Nullable
    @Override
    public Task findTaskByName(@NotNull final String name) throws ValidateExeption{
        @Nullable Task task = null;
        try {
            session = getJavaToMysql().getSqlSession(ITaskRepositoryMapper.class);
            mapper = session.getMapper(ITaskRepositoryMapper.class);
            task = mapper.findOne(name);
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return task;
    }

    @Override
    public void removeTask(@NotNull final Task task) throws ValidateExeption{
        if (Objects.isNull(mapper))
            return;

        try {
            session = getJavaToMysql().getSqlSession(ITaskRepositoryMapper.class);
            mapper = session.getMapper(ITaskRepositoryMapper.class);
            mapper.remove(task.getId());
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
    }

    @Override
    public void removeAllTasksByUserId(@NotNull final String userId) throws ValidateExeption{
        if (Objects.isNull(mapper))
            return;
        try {
            session = getJavaToMysql().getSqlSession(ITaskRepositoryMapper.class);
            mapper = session.getMapper(ITaskRepositoryMapper.class);
            for (Task task : mapper.findAllByUser(userId)) {
                if (userId.equals(task.getUserId()))
                    mapper.remove(task.getId());
            }
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
    }

    @Override
    public void removeAllTasks(@NotNull final Collection<Task> listTasks) throws ValidateExeption{
        if (Objects.isNull(mapper))
            return;
        try {
            session = getJavaToMysql().getSqlSession(ITaskRepositoryMapper.class);
            mapper = session.getMapper(ITaskRepositoryMapper.class);
            for (Task task : listTasks) {
                mapper.remove(task.getId());
            }
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
    }

    @NotNull
    @Override
    public Task renameTask(@NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException, ValidateExeption{
        if (Objects.nonNull(name) && Objects.nonNull(mapper)){
            @NotNull Task newTask = (Task) task.clone();
            newTask.setName(name);
            try {
                session = getJavaToMysql().getSqlSession(ITaskRepositoryMapper.class);
            mapper = session.getMapper(ITaskRepositoryMapper.class);
                mapper.merge(newTask);
                session.commit();
            }catch (Exception e){e.printStackTrace(); session.rollback();}
            finally {
                session.close();}
        }
        return task;
    }

    @Override
    public void changeProject(@NotNull final Task task, @Nullable final Project project) throws CloneNotSupportedException, ValidateExeption{
        if (Objects.nonNull(project) && Objects.nonNull(mapper)){
            @NotNull Task newTask = (Task) task.clone();
            newTask.setProjectId(project.getId());
            try {
                session = getJavaToMysql().getSqlSession(ITaskRepositoryMapper.class);
            mapper = session.getMapper(ITaskRepositoryMapper.class);
                mapper.merge(newTask);
                session.commit();
            }catch (Exception e){e.printStackTrace(); session.rollback();}
            finally {
                session.close();}
        }
    }

    @NotNull
    @Override
    public Collection<Task> findAllTasksByUserId(@NotNull final String userId, @NotNull final String projectId) throws ValidateExeption{
        @NotNull Collection<Task> list = new ArrayList<>();
        try {
            session = getJavaToMysql().getSqlSession(ITaskRepositoryMapper.class);
            mapper = session.getMapper(ITaskRepositoryMapper.class);
            list =  (mapper).findAllByUserIdAndProjectId(userId, projectId);
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return list;
    }

    @Override
    public @NotNull Collection<Task> findAllByUser(String userId) throws ValidateExeption {
        @NotNull Collection<Task> list = new ArrayList<>();
        try {
            session = getJavaToMysql().getSqlSession(ITaskRepositoryMapper.class);
            mapper = session.getMapper(ITaskRepositoryMapper.class);
            list = ((ITaskRepository) mapper).findAllByUser(userId);
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return list;
    }

    @NotNull
    @Override
    public Collection<Task> findAllTasks(@NotNull final String projectId) throws ValidateExeption{
        @NotNull Collection<Task> list = new ArrayList<>();
        try {
            session = getJavaToMysql().getSqlSession(ITaskRepositoryMapper.class);
            mapper = session.getMapper(ITaskRepositoryMapper.class);
            list =  mapper.findAll();
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
        return list;
    }

    @Override
    public void putTask(@NotNull final Task task) throws ValidateExeption{
        if (Objects.isNull(mapper))
            return;

        try {
            session = getJavaToMysql().getSqlSession(ITaskRepositoryMapper.class);
            mapper = session.getMapper(ITaskRepositoryMapper.class);
            if (Objects.isNull( mapper.findOneById(task.getId())  ))
                mapper.persist(task);
            else
                mapper.merge(task);
            session.commit();
        }catch (Exception e){e.printStackTrace(); session.rollback();}
        finally {
            session.close();}
    }

    @Override
    public void loadBinar() throws Exception {
    }

    @Override
    public void saveBinar() throws Exception {
    }

    @NotNull
    @Override
    public String collectTaskInfo(@NotNull final Task task) throws ValidateExeption {
        @NotNull String res = "";
        if (Objects.isNull(task.getDateStart()) || Objects.isNull(task.getDateFinish()) || Objects.isNull(getServiceLocator())
       )
            return res;

        res += "\n";
        res += "    Task [" + task.getName() + "]" +  "\n";
        res += "    Project " + getServiceLocator().getProjectService().findOneProjectById(task.getProjectId())  + "]" +  "\n";
        res += "    Status: " + task.getStatus() + "\n";
        res += "    Description: " + task.getDescription() + "\n";
        res += "    ID: " + task.getId() + "\n";
        res += "    Date start: " +  getServiceLocator().getFt().format(task.getDateStart() ) + "\n";
        res += "    Date finish: " + getServiceLocator().getFt().format(task.getDateFinish() ) + "\n";

        return res;
    }
    //Save-Load
    @Override public void saveJaxb(boolean formatXml) throws Exception { }
    @Override public void loadJaxb(boolean formatXml) throws Exception {}

    @Override public void saveFasterXml() throws Exception { }
    @Override public void loadFasterXml() throws Exception {}

    @Override public void saveFasterJson() throws Exception { }
    @Override public void loadFasterJson() throws Exception {}
}
