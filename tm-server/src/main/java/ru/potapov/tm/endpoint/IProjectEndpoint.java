package ru.potapov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IProjectEndpoint {
    @WebMethod int checkProjectSize(@NotNull Session session) throws ValidateExeption;
    @WebMethod @Nullable Project findOneProject(@WebParam @NotNull Session session, @WebParam @NotNull final String name) throws ValidateExeption;
    @WebMethod @Nullable Project findProjectByName(@WebParam @NotNull Session session, @WebParam @NotNull final String name) throws ValidateExeption;
    @WebMethod @Nullable Project findProjectByNameAndUserId(@WebParam @NotNull Session session, @WebParam @NotNull final String userId, @WebParam @NotNull final String name) throws ValidateExeption;
    @WebMethod @Nullable Project findOneProjectById(@WebParam @NotNull Session session, @WebParam @NotNull final String id) throws ValidateExeption;
    @WebMethod @Nullable Project findOneProjectByNameAndUserId(@WebParam @NotNull Session session, @WebParam @NotNull final String userId, @WebParam @NotNull final String id) throws ValidateExeption;
    @WebMethod @NotNull Collection<Project> getProjectCollection(@WebParam @NotNull Session session, @WebParam @NotNull final String userId) throws ValidateExeption;
    @WebMethod @NotNull Project renameProject(@WebParam @NotNull Session session, @WebParam @NotNull final Project project, @WebParam @NotNull final String name) throws CloneNotSupportedException, ValidateExeption;
    @WebMethod void removeAllProjectByUserId(@WebParam @NotNull Session session, @WebParam @NotNull final String userId) throws ValidateExeption;
    @WebMethod void removeAllProject(@WebParam @NotNull Session session, @WebParam Collection<Project> listProjects) throws ValidateExeption;
    @WebMethod void removeProject(@WebParam @NotNull Session session, @WebParam @NotNull final Project project) throws ValidateExeption;
    @WebMethod void putProject(@WebParam @NotNull Session session, @WebParam @NotNull final Project project) throws ValidateExeption;
    @NotNull String collectProjectInfo(@WebParam @NotNull Session session, @WebParam @NotNull final Project project, @WebParam @NotNull final String owener) throws ValidateExeption;

    @WebMethod void loadBinar(@Nullable Session session) throws Exception;
    @WebMethod void saveBinar(@Nullable Session session) throws Exception;

    @WebMethod void saveJaxb(@Nullable Session session, final boolean formatXml) throws Exception;
    @WebMethod void loadJaxb(@Nullable Session session, final boolean formatXml) throws Exception;

    @WebMethod void saveFasterXml(@Nullable Session session) throws Exception;
    @WebMethod void loadFasterXml(@Nullable Session session) throws Exception;

    @WebMethod void saveFasterJson(@Nullable Session session) throws Exception;
    @WebMethod void loadFasterJson(@Nullable Session session) throws Exception;
}
