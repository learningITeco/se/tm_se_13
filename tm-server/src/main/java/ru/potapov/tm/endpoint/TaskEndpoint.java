package ru.potapov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.*;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebService;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.potapov.tm.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractServiceEndpoint<Task> implements ITaskEndpoint {
    public TaskEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public int checkTaskSize(@NotNull Session session) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getTaskService().checkTaskSize();
    }

    @Nullable
    @Override
    public Task findTaskByName(@NotNull Session session, @NotNull final String name) throws ValidateExeption{
        return getServiceLocator().getTaskService().findTaskByName(name);
    }

    @Override
    public void removeTask(@NotNull Session session, @NotNull final Task task) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getTaskService().removeTask(task);
    }

    @Override
    public void removeAllTasksByUserId(@NotNull Session session, @NotNull final String userId) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getTaskService().removeAllTasksByUserId(userId);
    }

    @Override
    public void removeAllTasks(@NotNull Session session, @NotNull final Collection<Task> listTasks) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getTaskService().removeAllTasks(listTasks);
    }

    @NotNull
    @Override
    public Task renameTask(@NotNull Session session, @NotNull final Task task, @NotNull final String name) throws CloneNotSupportedException, ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getTaskService().renameTask(task,name);
    }

    @Override
    public void changeProject(@NotNull Session session, @NotNull final Task task, @Nullable final Project project) throws CloneNotSupportedException, ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getTaskService().changeProject(task, project);
    }

    @NotNull
    @Override
    public Collection<Task> findAllTasksByUserId(@NotNull Session session, @NotNull final String userId, @NotNull final String projectId) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getTaskService().findAllTasksByUserId(userId, projectId);
    }

    @Override
    public @NotNull Collection<Task> findAllByUser(@NotNull Session session, String userId) throws ValidateExeption {
        getServiceLocator().getSessionService().validSession(session);
        return ((ITaskRepository) getServiceLocator().getTaskService()).findAllByUser(userId);
    }

    @NotNull
    @Override
    public Collection<Task> findAllTasks(@NotNull Session session, @NotNull final String projectId) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        return getServiceLocator().getTaskService().findAllTasks(projectId);
    }

    @Override
    public void putTask(@NotNull Session session, @NotNull final Task task) throws ValidateExeption{
        getServiceLocator().getSessionService().validSession(session);
        getServiceLocator().getTaskService().putTask(task);

    }

    @Override
    public void loadBinar(@NotNull Session session) throws Exception {
        getServiceLocator().getSessionService().validSession(session);
    }

    @Override
    public void saveBinar(@NotNull Session session) throws Exception {
        getServiceLocator().getSessionService().validSession(session);
    }

    @NotNull
    @Override
    public String collectTaskInfo(@NotNull Session session, @NotNull final Task task) throws ValidateExeption {
        getServiceLocator().getSessionService().validSession(session);
        return  getServiceLocator().getTaskService().collectTaskInfo(task);
    }
    //Save-Load
    @Override public void saveJaxb(@NotNull Session session, boolean formatXml) throws Exception { }
    @Override public void loadJaxb(@NotNull Session session, boolean formatXml) throws Exception {}

    @Override public void saveFasterXml(@NotNull Session session) throws Exception { }
    @Override public void loadFasterXml(@NotNull Session session) throws Exception {}

    @Override public void saveFasterJson(@NotNull Session session) throws Exception { }
    @Override public void loadFasterJson(@NotNull Session session) throws Exception {}
}
