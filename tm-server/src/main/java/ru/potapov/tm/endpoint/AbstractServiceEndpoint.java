package ru.potapov.tm.endpoint;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IEntity;
import ru.potapov.tm.api.IRepository;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.dto.DataXml;
import ru.potapov.tm.entity.Entity;
import ru.potapov.tm.entity.Project;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractServiceEndpoint<T extends Entity> {
    //@Nullable private IRepository<T> repository;
    @Nullable private ServiceLocator serviceLocator;
    @Nullable private String className = ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getTypeName().toString().split("\\.")[((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getTypeName().toString().split("\\.").length-1];

    public AbstractServiceEndpoint(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

//    public void saveBinar() throws Exception{
//        @NotNull final File file = new File("data-"+ className +".binar");
//        @NotNull final ObjectOutputStream inputStream = new ObjectOutputStream(new FileOutputStream(file));
//
//        @NotNull final Data data = new Data();
//        data.setProjectMap( getServiceLocator().getProjectService().getProjectMapRepository() );
//        data.setTaskMap( getServiceLocator().getTaskService().getTaskMapRepository() );
//
//        inputStream.writeObject( data );
//        inputStream.close();
//    }
//
//    public void loadBinar() throws Exception{
//        @NotNull final File file = new File("data-"+ className +".binar");
//        if (!file.canRead()){
//            getServiceLocator().getTerminalService().printlnArbitraryMassage("File cannot be opened");
//            return;
//        }
//
//        @NotNull final ObjectInputStream inputStream = new ObjectInputStream( new FileInputStream(file) );
//        @NotNull final Object dataObj = inputStream.readObject();
//        if (dataObj instanceof Data){
//            @NotNull final Data data = (Data) dataObj;
//            getServiceLocator().getProjectService().setProjectMapRepository(data.getProjectMap());
//            getServiceLocator().getTaskService().setTaskMapRepository(data.getTaskMap());
//        }
//    }
//
//    public abstract void saveJaxb(boolean formatXml) throws Exception ;
//    public abstract void loadJaxb(boolean formatXml) throws Exception ;
//
//    public abstract void  saveFasterXml() throws Exception ;
//    public abstract void loadFasterXml() throws Exception ;
//
//    public abstract void saveFasterJson() throws Exception ;
//    public abstract void loadFasterJson() throws Exception;
}
