package ru.potapov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface ISessionEndpoint {
    @WebMethod boolean validSession(@WebParam @NotNull Session session) throws ValidateExeption;
    @WebMethod void addSession(@WebParam @NotNull Session session, String userId) throws ValidateExeption;
    @WebMethod void removeSession(@WebParam @NotNull Session session) throws ValidateExeption;
}
