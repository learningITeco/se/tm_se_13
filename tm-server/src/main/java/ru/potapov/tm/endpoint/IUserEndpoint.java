package ru.potapov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.dto.MyMap;
import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.util.ValidateExeption;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.security.MessageDigest;
import java.util.Collection;

@WebService
public interface IUserEndpoint {
    @WebMethod @NotNull User createUser(@WebParam @NotNull final String name, @WebParam @NotNull final String hashPass, @NotNull final RoleType role);
    @WebMethod @Nullable User getUserByNamePass(@WebParam @NotNull final String name, @WebParam @NotNull final String pass) throws Exception;
    @WebMethod @Nullable User getUserById(@WebParam @NotNull Session session, @NotNull final String id) throws ValidateExeption;
    @WebMethod @NotNull User changePass(@WebParam @NotNull Session session, @NotNull final User user, @NotNull final String newHashPass) throws CloneNotSupportedException, ValidateExeption;
    @WebMethod void putUser(@WebParam @NotNull Session session, @WebParam @NotNull final User user) throws ValidateExeption;
    @WebMethod @NotNull String collectUserInfo(@WebParam @NotNull Session session, @WebParam @NotNull final User user) throws ValidateExeption;
    @WebMethod @Nullable RoleType getUserRole(@WebParam @NotNull Session session, @WebParam @NotNull final User user) throws ValidateExeption;
    @WebMethod boolean isAdministrator(@WebParam @NotNull Session session, @WebParam @NotNull final User user) throws ValidateExeption;
    void saveAllUsers() throws Exception;
    @WebMethod @Nullable User getUserByName(@WebParam @NotNull final String name);
    @WebMethod @NotNull Collection<User> getUserCollection();

}
