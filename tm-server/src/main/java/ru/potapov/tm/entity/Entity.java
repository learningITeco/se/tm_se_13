package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IEntity;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public abstract class Entity implements IEntity, Cloneable, Serializable {
    @Nullable private String id = UUID.randomUUID().toString();
}
