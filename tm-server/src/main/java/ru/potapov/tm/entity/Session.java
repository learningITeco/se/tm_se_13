package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class Session extends Entity implements Cloneable{
    @NotNull private String userId        = "";
    @NotNull private String signature     = "";
    @NotNull private long dateStamp       = new Date().getTime();
    //@NotNull private final String hashPass      = "";

    @Override
    public @Nullable String getName() {
        return signature;
    }

    @Override
    public Session clone() throws CloneNotSupportedException {
        try {
            return (Session)super.clone();
        }catch (CloneNotSupportedException e){ return null;}
    }
}
