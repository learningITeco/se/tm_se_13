package ru.potapov.tm.util;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManagerFactory;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class HibernateToMySql {
    @NotNull private static Properties propertyService   = new Properties();
    @Nullable private FileInputStream stream;

    public HibernateToMySql() {
        String resource = "mybatis-config.xml";
        try {
            stream = new FileInputStream(resource);
            propertyService.load(stream);
        }catch (Exception e){e.printStackTrace();}
    }

    private EntityManagerFactory factory() {
        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getProperty("driver", "com.mysql.jdbc.Driver"));
        settings.put(Environment.URL, propertyService.getProperty("url", "jdbc:mysql://localhost:3306/tm"));
        settings.put(Environment.USER, propertyService.getProperty("user", "root"));
        settings.put(Environment.PASS, propertyService.getProperty("password", "root"));
        settings.put(Environment.DIALECT,
                "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        final StandardServiceRegistryBuilder registryBuilder
                = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
//        sources.addAnnotatedClass(Task.class);
//        sources.addAnnotatedClass(Project.class);
//        sources.addAnnotatedClass(User.class);
//        sources.addAnnotatedClass(Session.class);
//        sources.addAnnotatedClass(Cat.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }
}
