package ru.potapov.tm.util;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class JavaToMysql {
    @NotNull private static Properties propertyService   = new Properties();
    @Nullable private FileInputStream stream;

    public JavaToMysql() {
        String resource = "mybatis-config.xml";
        try {
            stream = new FileInputStream(resource);
            propertyService.load(stream);
        }catch (Exception e){e.printStackTrace();}
    }

    @NotNull
    //@SneakyThrows
    public SqlSession getSqlSession(Class<? extends Object> type){
        SqlSessionFactory build = getSqlSessionFactory();
        SqlSession sqlSession = build.openSession();
        sqlSession.getConfiguration().addMapper(type);
        return sqlSession;
    }

    @NotNull
    //@SneakyThrows
    public SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String user = propertyService.getProperty("user", "root");
        @Nullable final String password = propertyService.getProperty("password", "root");
        @Nullable final String url = propertyService.getProperty("url", "jdbc:mysql://localhost:3306/tm");
        @Nullable final String driver = propertyService.getProperty("driver", "com.mysql.jdbc.Driver");
        final DataSource dataSource = new PooledDataSource(driver, url, user, password);
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        return new SqlSessionFactoryBuilder().build(configuration);
    }
}
