package ru.potapov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.api.IEntity;
import ru.potapov.tm.entity.Entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class MyMap {
    @Nullable protected  Map<String, ? extends Entity> map;

    public MyMap(@Nullable Map<String, ? extends Entity> map) {
        this.map = map;
    }
}


